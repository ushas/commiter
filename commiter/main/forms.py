from django import forms


class UserForm(forms.Form):
    name = forms.CharField()
    id = forms.IntegerField()


class TokenForm(forms.Form):
    token = forms.CharField(label='Gitlab токен', required=True)
